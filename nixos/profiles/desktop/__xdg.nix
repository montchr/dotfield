{
  xdg = {
    mime.enable = true;
    icons.enable = true;
    portal.enable = true;
  };
}

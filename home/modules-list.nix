[
  ./modules/cod.nix
  ./modules/syncthing.nix

  ./modules/dotfield/paths.nix
  ./modules/dotfield/whoami.nix
  ./modules/theme/default.nix

  ./modules/programs/bash/trampoline.nix
  ./modules/programs/emacs/emacs-init-defaults.nix
  ./modules/programs/emacs/emacs-init.nix
  ./modules/programs/emacs/emacs-notmuch.nix
  ./modules/programs/liquidprompt.nix
  ./modules/programs/tealdeer.nix

  ./modules/services/emacs.nix
]
